# Mon plus beau portfolio

# Lien version en ligne : 

http://portfolio-julien.surge.sh/

## Voici mon portfolio dans le cadre de l'évaluation des compétences C1 C2 C3 

## Le projet comprend un portfolio personnalisé, respectant les contraintes demandées : 


- Le portfolio est responsive.

- L'ecoindex est A ou B.

- Le Performance Score est au moins de 80%.

- Le site ne comporte aucune erreur d'accessibilité.

- Le code est propre est indenté. Tous les critères de performance des compétences C1, C2 et C3 présentées dans le référentiel.

## Fonctionnement 

Le portolio ne possède qu'une seul page , avec une barre de navigation afin de naviguer à travers le site en fonction de ces différentes partits.

Il possède également un formulaire permettant de renseigner son adresse mail, envoyer un message, et différents liens redirigeant vers divers projets réalisés.
      
